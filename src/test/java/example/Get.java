package example;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Get {
    @Test
    @DisplayName("Проверка метода GET SINGLE <RESOURCE> NOT FOUND")
    public void successGetSingleUserStatusCode() throws IOException {
        URL url = new URL("https://reqres.in/api/unknown/23");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        System.out.println(connection.getResponseCode());
    }


    @Test
    @DisplayName("Проверка кода GET SINGLE <RESOURCE> NOT FOUND")
    public void Get() throws IOException, InterruptedException, URISyntaxException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/unknown/23"))
                .GET().build();

        HttpResponse response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

        assertEquals(404, response.statusCode(), "status code корректный");
    }
}